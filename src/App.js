import React, { useState, useRef, useEffect } from "react";
import "./App.css";
import ReactCrop, { centerCrop, makeAspectCrop } from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import { canvasPreview } from "./canvasPreview";

// This is to demonstate how to make and center a % aspect crop
// which is a bit trickier so we use some helper functions.
function centerAspectCrop(mediaWidth, mediaHeight, aspect) {
  return centerCrop(
    makeAspectCrop(
      {
        unit: "%",
        width: 90,
      },
      aspect,
      mediaWidth,
      mediaHeight
    ),
    mediaWidth,
    mediaHeight
  );
}

function App() {
  const [imgSrc, setImgSrc] = useState("");
  const previewCanvasRef = useRef(null);
  const imgRef = useRef(null);
  const [crop, setCrop] = useState();
  const [completedCrop, setCompletedCrop] = useState();
  const [scale, setScale] = useState(1);
  const [rotate, setRotate] = useState(0);
  const [aspect, setAspect] = useState(16 / 9);

  useEffect(() => {
    if (
      completedCrop?.width &&
      completedCrop?.height &&
      imgRef.current &&
      previewCanvasRef.current
    ) {
      // We use canvasPreview as it's much faster than imgPreview.
      canvasPreview(
        imgRef.current,
        previewCanvasRef.current,
        completedCrop,
        scale,
        rotate
      );

      // to get base64 url use and set ( previewCanvasRef?.current?.toDataURL())
    }
  }, [completedCrop, scale, rotate]);

  function onSelectFile(e) {
    if (e.target.files && e.target.files.length > 0) {
      setCrop(undefined); // Makes crop preview update between images.
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        setImgSrc(reader.result?.toString() || "")
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  }

  function onImageLoad(e) {
    if (aspect) {
      const { width, height } = e.currentTarget;
      setCrop(centerAspectCrop(width, height, aspect));
    }
  }

  const handleCropChange = (xyz, crops) => {
    setCrop(crops);
    setCompletedCrop(xyz);
    if (
      completedCrop?.width &&
      completedCrop?.height &&
      imgRef.current &&
      previewCanvasRef.current
    ) {
      // We use canvasPreview as it's much faster than imgPreview.
      canvasPreview(
        imgRef.current,
        previewCanvasRef.current,
        xyz,
        scale,
        rotate
      );
    }
  };

  return (
    <div className="mt-10 ">
      <h2 className="font-bold text-blue-500 text-xl text-center">
        Image : Drag || upload && Crop
      </h2>
      <div class="py-5 bg-white px-2">
        <div class="max-w-md mx-auto rounded-lg overflow-hidden md:max-w-xl">
          <div class="md:flex">
            <div class="w-full p-3">
              <div class="relative border-dotted h-20 rounded-lg border-dashed border-2 border-blue-700 bg-gray-100 flex justify-center items-center">
                <div class="absolute">
                  <div class="flex flex-col items-center">
                    <i class="fa fa-folder-open fa-4x text-blue-700"></i>
                    <span class="block text-gray-400 font-normal">
                      Attach you files here
                    </span>
                  </div>
                </div>

                <input
                  type="file"
                  className="h-full w-full opacity-0 cursor-pointer"
                  onChange={onSelectFile}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="flex justify-evenly">
        {imgSrc && (
          <ReactCrop
            crop={crop}
            onChange={(_, percentCrop) => handleCropChange(_, percentCrop)}
            onComplete={(c) => setCompletedCrop(c)}
            aspect={false}
            style={{
              width: "50%",
              height: "500px",
              marginBottom: 50,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <img
              ref={imgRef}
              class="fit-cover"
              alt="Crop me"
              src={imgSrc}
              style={{
                transform: `scale(${scale}) rotate(${rotate}deg)`,
                width: "100%",
                height: "100%",
                objectFit: "contain",
              }}
              onLoad={onImageLoad}
            />
          </ReactCrop>
        )}

        {!!completedCrop && (
          <div
            style={{
              width: "40%",
              height: "500px",
              marginBottom: 50,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              background: "lgray",
            }}
          >
            <canvas
              ref={previewCanvasRef}
              style={{
                objectFit: "contain",
                width: "100%",
                height: "100%",
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
